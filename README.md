A free to use update tool which checks phish.sinking.yachts database.
It adds all domains found after custom domains are added. If not needed they can be commented or removed.

**added a processbar**

To install this package you can use one of the below.

```text
git clone https://git.codingvm.codes/ultimateplayer1999/Golang-phish-update.git (Use this one if you do not have SSH keys linked.)

git clone git@git.codingvm.codes:ultimateplayer1999/Golang-phish-update.git
```

To install the needed external packages use the following commands.
```text
go get github.com/joho/godotenv
go get github.com/cheggaaa/pb/v3
go get github.com/go-sql-driver/mysql
```

The following code is needed in a .env. This is for the DB connection.

```text
DB_HOST=
DB_PORT=
DATABASE=
DB_USER=
DB_PASS=
```

This is based on the phish table. Table can also be changed if needed.

Execution time is *1h26m19.334785368s*. This was for **18876 records**.

Sample output with provided code:

```text
Script started at 12-01-2023 13:49:59
Deletion: Number of rows affected: 59
Altering: Number of rows affected: 0
Insert: Number of rows affected: 1
Insert: Number of rows affected: 1
Status code dbsize endpoint:  200
12-01-2023 13:50:00: Initial database table setup done
Status code:  200
18876 / 18876 [-------------------------------------------------------------------------------------------->] 100%
Total number of rows affected: 18876
Recent changes recieved and has been added to the database. Removed domains deleted from database. Reorded domains.
Script ended at 12-01-2023 15:16:19
Time until completion:  1h26m19.334785368s
```

# DISCLAIMER
The binary may need to be rebuild. As it is build on Ubuntu and it may also depends on how you installed Golang.
